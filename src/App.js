import React from 'react';
import './App.css';
import Preview from './components/Preview';

function App() {
  return (
    <div className="App">
      <header className="App-header">&nbsp;</header>
      <Preview />
    </div>
  );
}

export default App; 