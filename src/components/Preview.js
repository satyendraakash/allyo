import React from 'react'
import './Preview.css';
import axios from 'axios';
import PreviewSection from './PreviewSection';

class Preview extends React.Component{
    constructor(){
        super();
        this.state  =   {
            previewData:null
        }
    }

    componentDidMount(){
        this.timerObj   =   null;
    }

    handleSearch    =   (searchString)=>{
        let urlArr = searchString.match(/(((https?:\/\/)|(www\.))[^\s]+)/gi);
        if(urlArr){
            let urlString   =   urlArr[0];
            if(urlString.indexOf("http://") === -1 && urlString.indexOf("https://") === -1){
                urlString   =   "http://"+urlString;
            }
            axios.post("https://allyo-server.herokuapp.com/preview/", {url:urlString}).then((resp)=>{
                if(resp.data){
                    this.setState({previewData:{...resp.data, linkURL:urlString}});
                }
            }).catch((err)=>{
                console.log(err);
            });
        }else{
            this.setState({previewData:null});
        }
    }

    changeHandle  =   (event)=>{
        clearTimeout(this.timerObj);
        let value   =   event.target.value;
        if(value){
            this.timerObj   =   setTimeout(() => {
                this.handleSearch(value);
            }, 1000);
        }
    }

    render(){
        return (
            <div className="inputSection">
                <input type="text" onChange={(e)=>{this.changeHandle(e)}} placeholder="Checkout https://www.airbnb.com/ --" />
                {this.state.previewData && <PreviewSection data={this.state.previewData} />}
            </div>
        );
    }
}

export default Preview;
