import React from 'react';

class PreviewSection extends React.Component{
    constructor(props){
        super(props);
        this.state = {dataobj:{}}
    }

    static getDerivedStateFromProps(props, state){
       return {
            title:props.data.title,
            description:props.data.description,
            thumbImage:props.data.thumbImage,
            hostName:props.data.hostName,
            linkURL:props.data.linkURL
        };
    }

    render(){
        const {title, description, thumbImage, hostName, linkURL}   =   this.state;
        console.log(this.state.title);
        return (
            <div className="previewbox">
                {thumbImage && <div className="imgbox"><a href={linkURL}><img src={thumbImage} alt={title} width={250} /></a></div>}
                {title && <div className="title"><a href={linkURL}>{title}</a></div>}
                {description && <div className="desc">{description}</div>}
                {hostName && <div className="host"><a href={linkURL}>{hostName}</a></div>}
            </div> 
        );
    }
}

export default PreviewSection; 